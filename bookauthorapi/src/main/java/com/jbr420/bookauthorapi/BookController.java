package com.jbr420.bookauthorapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class BookController {
    @GetMapping("/books")
    public static ArrayList<Book> getBookList() {
        Author author1 = new Author("Lan", "lan@gmail.com", 'f');
        Author author2 = new Author("Hoa", "hoa@gmail.com", 'f');
        Author author3 = new Author("Nam", "nam@gmail.com", 'm');
        System.out.println("author1 la: " + author1.toString());
        System.out.println("author2 la: " + author2.toString());
        System.out.println("author3 la: " + author3.toString());
        Book book1 = new Book("Harry Potter", author1, 90000, 7);
        Book book2 = new Book("Twilight", author2, 110000, 9);
        Book book3 = new Book("Animal Farm", author3, 130000, 6);
        System.out.println("book1 la: " + book1.toString());
        System.out.println("book2 la: " + book2.toString());
        System.out.println("book3 la: " + book3.toString());
        ArrayList<Book> bookList = new ArrayList<Book>();
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        return bookList;
    } 
}
